package cmd

import "flag"

type Flags struct {
	ConfigPath string
}

var flags *Flags

func Init() {
	flags = &Flags{}
	flag.StringVar(&flags.ConfigPath, "config-path", "internal/app/sender/config/config.yaml", "path to configuration file")
}

func Parse() *Flags {
	flag.Parse()
	return flags
}
