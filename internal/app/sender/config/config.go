package config

type Config struct {
	Server struct {
		Port string `yaml:"port", envconfig:"SERVER_PORT"`
	} `yaml:"server"`
	Remote struct{
		Host string `yaml:"host", envconfig:"REMOTE_HOST"`
		Port string `yaml:"port", envconfig:"REMOTE_PORT"`
	} `yaml: "remote"`
}

func TestConfig() *Config {
	c := &Config{}
	c.Server.Port = "8080"
	c.Remote.Host = "localhost"
	c.Remote.Port = "8082"

	return c
}