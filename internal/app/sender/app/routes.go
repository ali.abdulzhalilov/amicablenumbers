package app

import (
	"github.com/go-chi/render"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"test-api/internal/app/sender/app/service"
	"time"
)

var (
	remoteService service.RemoteService
)

func Routes(remoteServiceInstance service.RemoteService) *mux.Router {
	remoteService = remoteServiceInstance

	router := mux.NewRouter()
	router.HandleFunc("/", sayHello)
	router.Handle("/magic/{number}", magicNumberHandler())
	router.Handle("/magic/{from}/{to}", batchMagicNumberHandler())
	return router
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]string)
	response["message"] = "Hello, world!"
	render.JSON(w, r, response)
}

type MagicResult struct {
	Input      string `json:"input"`
	Output     string `json:"output"`
	IsAmicable string `json:"isAmicable"`
	Error      string `json:"error"`
}

func magicNumberHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		magicResult, err := remoteService.GetMagic(vars["number"])
		if err != nil {
			render.JSON(w, r, err.Error())
			return
		}

		render.JSON(w, r, magicResult)
	})
}

func batchMagicNumberHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		now := time.Now()
		batch, err := remoteService.GetBatchMagicParallel(vars["from"], vars["to"])
		if err != nil {
			log.Println(err)
			render.JSON(w, r, err.Error())
			return
		}
		log.Println(time.Since(now).Seconds())

		render.JSON(w, r, batch)
	})
}
