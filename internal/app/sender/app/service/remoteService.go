package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/eapache/channels"
	"net/http"
	"strconv"
	"sync"
	"sync/atomic"
)

type RemoteService interface {
	SendRequest(number string) (*http.Response, error)
	GetMagic(number string) (*MagicResult, error)
	GetBatchMagic(fromNumber string, toNumber string) (*BatchMagicResult, error)
	GetBatchMagicParallel(fromNumber string, toNumber string) (*BatchMagicResult, error)
}

type RemoteServiceImpl struct {
	RemoteHost string
	RemotePort string
}

type MagicResult struct {
	Input      string `json:"input"`
	Output     string `json:"output"`
	IsAmicable string `json:"isAmicable"`
	Error      string `json:"error"`
}

type BatchMagicResult struct {
	Count   int            `json:"count"`
	Results []*MagicResult `json:"results"`
}

func NewRemoteService(remoteHost string, remotePort string) *RemoteServiceImpl {
	return &RemoteServiceImpl{
		RemoteHost: remoteHost,
		RemotePort: remotePort,
	}
}

func (remoteService *RemoteServiceImpl) SendRequest(numberAsString string) (*http.Response, error) {
	endpoint := fmt.Sprintf("http://%s:%s/magic/%s", remoteService.RemoteHost, remoteService.RemotePort, numberAsString)
	return http.Get(endpoint)
}

func (remoteService *RemoteServiceImpl) GetMagic(numberAsString string) (*MagicResult, error) {
	resp, err := remoteService.SendRequest(numberAsString)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	magicResult := &MagicResult{}
	err = json.NewDecoder(resp.Body).Decode(&magicResult)
	if err != nil {
		return nil, err
	}

	return magicResult, nil
}

func (remoteService *RemoteServiceImpl) GetBatchMagic(fromAsString string, toAsString string) (*BatchMagicResult, error) {
	fromAsInt, errFrom := strconv.Atoi(fromAsString)
	toAsInt, errTo := strconv.Atoi(toAsString)
	switch {
	case errFrom != nil:
		return nil, errFrom
	case errTo != nil:
		return nil, errTo
	case fromAsInt >= toAsInt:
		return nil, errors.New("toNumber should be greater than fromNumber")
	}

	results := make([]*MagicResult, 0)

	for i := fromAsInt; i < toAsInt; i++ {
		magicResult, err := remoteService.GetMagic(strconv.Itoa(i))
		if err != nil {
			return nil, err
		}

		results = append(results, magicResult)
	}

	return &BatchMagicResult{Results: results, Count: len(results)}, nil
}

func (remoteService *RemoteServiceImpl) GetBatchMagicParallel(fromAsString string, toAsString string) (*BatchMagicResult, error) {
	fromAsInt, errFrom := strconv.ParseInt(fromAsString, 10, 64)
	toAsInt, errTo := strconv.ParseInt(toAsString, 10, 64)
	switch {
	case errFrom != nil:
		return nil, errFrom
	case errTo != nil:
		return nil, errTo
	case fromAsInt >= toAsInt:
		return nil, errors.New("toNumber should be greater than fromNumber")
	}

	var jobs channels.Channel = channels.NewInfiniteChannel()
	magicResults := make([]*MagicResult, toAsInt-fromAsInt)
	var jobsCounter int64

	var wg sync.WaitGroup
	for w := 1; w <= 25; w++ {
		wg.Add(1)
		go magic(w, remoteService, jobs, magicResults, &jobsCounter, &wg)
	}

	var j int64
	for j = 0; j < toAsInt-fromAsInt; j++ {
		jobs.In() <- fromAsInt + j
	}
	jobs.Close()

	wg.Wait()

	return &BatchMagicResult{Results: magicResults}, nil
}

func magic(id int, service *RemoteServiceImpl, jobs channels.Channel, results []*MagicResult, jobsCounter *int64, wg *sync.WaitGroup) {
	defer wg.Done()

	for thing := range jobs.Out() {
		number := thing.(int64)

		magicResult, err := service.GetMagic(strconv.FormatInt(number, 10))
		index := atomic.AddInt64(jobsCounter, 1) - 1

		var whatToWrite MagicResult
		if err == nil {
			whatToWrite = *magicResult
		} else {
			whatToWrite = MagicResult{Error: err.Error()}
		}

		results[index] = &whatToWrite
		//fmt.Println("id", id, "wrote ", whatToWrite, " at ", index, "so", results[index])
	}
}
