package app

import (
	"github.com/gorilla/mux"
	"net/http"
	"test-api/internal/app/sender/app/service"
	"test-api/internal/app/sender/config"
	"time"
)

type App struct {
	config *config.Config
	Router *mux.Router
}

func New(configuration *config.Config) *App {
	a := App{}
	a.config = configuration

	// config DB

	// get model

	remoteService := service.NewRemoteService(configuration.Remote.Host, configuration.Remote.Port)
	a.Router = Routes(remoteService)

	return &a
}

func (a *App) Start() error {
	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 600 * time.Second,
		Addr:         ":" + a.config.Server.Port,
		Handler:      a.Router,
	}

	return srv.ListenAndServe()
}
