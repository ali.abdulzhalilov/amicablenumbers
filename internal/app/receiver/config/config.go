package config

type Config struct {
	Server struct {
		Port string `yaml:"port", envconfig:"SERVER_PORT"`
	} `yaml:"server"`
}

func TestConfig() *Config {
	c := &Config{}
	c.Server.Port = "8080"

	return c
}