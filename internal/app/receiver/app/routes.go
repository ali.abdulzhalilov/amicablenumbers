package app

import (
	"github.com/go-chi/render"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"test-api/internal/app/receiver/app/service"
)

func Routes(amicableService *service.AmicableService) *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", sayHello)
	router.Handle("/magic/{number}", magicNumberHandler(*amicableService))
	return router
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]string)
	response["message"] = "Hello, world!"
	render.JSON(w, r, response)
}

func magicNumberHandler(amicableService service.AmicableService) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response := make(map[string]string)

		vars := mux.Vars(r)
		number, isAmicable, err := amicableService.GetAmicableNumberFromString(vars["number"])

		response["input"] = vars["number"]
		response["output"] = strconv.FormatInt(number, 10)
		response["isAmicable"] = strconv.FormatBool(isAmicable)
		if err != nil {
			response["error"] = err.Error()
		}

		render.JSON(w, r, response)
	})
}
