package app

import (
	"github.com/gorilla/mux"
	"net/http"
	"test-api/internal/app/receiver/app/service"
	"test-api/internal/app/receiver/config"
)

type App struct {
	config *config.Config
	Router *mux.Router
}

func New(configuration *config.Config) *App {
	a := App{}
	a.config = configuration

	var amicableService service.AmicableService = service.NewAmicableService()

	// config DB

	// get model

	a.Router = Routes(&amicableService)

	return &a
}

func (a *App) Start() error {
	return http.ListenAndServe(
		":"+a.config.Server.Port,
		a.Router)
}
