package service

import (
	"errors"
	"math"
	"strconv"
)

type AmicableService interface {
	GetAmicableNumberFromString(input string) (int64, bool, error)
	GetAmicableNumberFromInt(input int64) (int64, bool, error)
}

type AmicableServiceImpl struct{}

func NewAmicableService() *AmicableServiceImpl {
	return &AmicableServiceImpl{}
}

func (service *AmicableServiceImpl) GetAmicableNumberFromString(input string) (int64, bool, error) {
	inputAsNumber, err := strconv.ParseInt(input, 10, 64)
	if err != nil {
		return 0, false, err
	}

	return service.GetAmicableNumberFromInt(inputAsNumber)
}

func (service *AmicableServiceImpl) GetAmicableNumberFromInt(input int64) (int64, bool, error) {
	switch {
	case input <= 0:
		return 0, false, errors.New("number should be greater than 0")
	case input > math.MaxInt64:
		return 0, false, errors.New("number should be less than " + string(math.MaxInt64))
	}

	possibleFriendlyNumber := sumOfDividers(input)

	return possibleFriendlyNumber, sumOfDividers(possibleFriendlyNumber) == input, nil
}

func sumOfDividers(number int64) int64 {
	var sum int64 = 0
	var i int64 = 0

	for i = 1; i < number/2+1; i++ {
		if number%i == 0 {
			sum += i
		}
	}

	return sum
}
