package config

import (
	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
	"log"
	"os"
)

func ReadFile(configPath string, cfg interface{}) {
	f, err := os.Open(configPath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("read configuration from file " + configPath)
}

func ReadEnv(cfg interface{}) {
	err := envconfig.Process("", cfg)
	if err != nil {
		log.Fatal(err)
	}
}