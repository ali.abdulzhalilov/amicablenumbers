module test-api

go 1.13

require (
	github.com/eapache/channels v1.1.0
	github.com/eapache/queue v1.1.0 // indirect
	github.com/go-chi/render v1.0.1
	github.com/gorilla/mux v1.7.4
	github.com/kelseyhightower/envconfig v1.4.0
	gopkg.in/yaml.v2 v2.2.8
)
