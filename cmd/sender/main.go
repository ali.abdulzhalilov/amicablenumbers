package main

import (
	"log"
	"test-api/internal/app/sender/app"
	"test-api/internal/app/sender/cmd"
	"test-api/internal/app/sender/config"
	configUtil "test-api/internal/pkg/config"
)

func init() {
	cmd.Init()
}

func main() {
	flags := cmd.Parse()

	var cfg config.Config
	configUtil.ReadFile(flags.ConfigPath, &cfg)
	configUtil.ReadEnv(&cfg)

	server := app.New(&cfg)

	log.Fatal(server.Start())
}


